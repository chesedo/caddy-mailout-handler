import fetch from 'node-fetch';
import Message from './message';

/**
 * Notifier is implemented by user to implement their particular UI needs.
 * It functions as a callback.
 */
type Notifier = (message: Message) => void;

/**
 * IData is the most basic data that can be send
 */
interface IData {
  email: string;
  name?: string;
  [propName: string]: any;
}

export default class Mailout {
  /**
   * Mailout is the handler for the mailout call. It uses AJAX to make the call.
   *
   * @param endpoint - The endpoint mailout is at
   * @param notifier - The user's notifier that handles the result
   */
  constructor(private endpoint: string, private notifier: Notifier) {}

  /**
   * send sends data to the endpoint
   *
   * @param data - The data to send
   */
  public send(data: IData): Promise<void> {
    return fetch(this.endpoint, {
      body: Object.entries(data)
        .map(([key, val]: [string, string]) => `${key}=${val}`)
        .join('&'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
    })
      .then(res => res.json())
      .then(response => {
        let m: Message;

        if (!response.code) {
          m = new Message(400);
        } else {
          m = new Message(response.code, response.error);
        }

        this.notifier(m);
      })
      .catch(error => this.notifier(new Message(500, `Unexpected error:\n\t${error}`)));
  }
}
