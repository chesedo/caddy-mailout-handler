import Message from './message';

describe('Message', () => {
  let mFailed: Message;
  let mPassed: Message;

  beforeAll(() => {
    mFailed = new Message(404, 'Not Found');
    mPassed = new Message(200, '');
  });

  test('hasMailed() to be true on code 200 only', () => {
    expect(mFailed.hasMailed()).toBeFalsy();
    expect(mPassed.hasMailed()).toBeTruthy();
  });
});
