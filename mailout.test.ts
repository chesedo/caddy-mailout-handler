import Mailout from './mailout';

// Mock fetch
jest.mock('node-fetch');
const fetchMock = require('node-fetch');

fetchMock.mockImplementation((uri: string) => {
  let p = new Promise((resolve, reject) => {
    // Simulate error
    if (uri === 'wrong') {
      reject('wrong uri');
    }

    // Handle possible responses
    let data = {};
    if (uri.indexOf('code/') === 0) {
      switch (uri.substr(5)) {
        case '422':
          data = { code: 422, error: 'Invalid email address: ""' };
          break;
        case '200':
          data = { code: 200 };
          break;
      }

      resolve({
        json: function() {
          return data;
        },
      });
    }
  });

  return p;
});

describe('Mailout', () => {
  let notifierMock = jest.fn();

  afterEach(() => {
    // Clear mocks
    notifierMock.mockClear();
    fetchMock.mockClear();
  });

  test('should fail on wrong url', async () => {
    let m = new Mailout('wrong', notifierMock);
    await m.send({ email: '' });

    expect.assertions(1);
    expect(notifierMock).toBeCalledWith({ code: 500, mes: 'Unexpected error:\n\twrong uri' });
  });

  test('should fail on no email', async () => {
    let m = new Mailout('code/422', notifierMock);
    await m.send({ email: '' });

    expect.assertions(1);
    expect(notifierMock).toBeCalledWith({ code: 422, mes: 'Invalid email address: ""' });
  });

  test('should fail on unexpected resolve having no json()', async () => {
    let m = new Mailout('code/no-json', notifierMock);
    await m.send({ email: '' });

    expect.assertions(1);
    expect(notifierMock).toBeCalledWith({ code: 400, mes: '' });
  });

  test('should pass when no error', async () => {
    let m = new Mailout('code/200', notifierMock);
    await m.send({ email: 't@te.st' });

    expect.assertions(2);
    expect(notifierMock).toBeCalledWith({ code: 200, mes: '' });
    expect(fetchMock.mock.calls[0][1].body).toBe('email=t@te.st');
  });

  test('should pass all data to fetch body', async () => {
    let m = new Mailout('code/200', notifierMock);
    await m.send({ email: 't@te.st', name: 'Joe', subscribe: false });

    expect.assertions(1);
    expect(fetchMock.mock.calls[0][1].body).toBe('email=t@te.st&name=Joe&subscribe=false');
  });
});
