[![pipeline status](https://gitlab.com/chesedo/caddy-mailout-handler/badges/master/pipeline.svg)](https://gitlab.com/chesedo/caddy-mailout-handler/commits/master)
[![coverage report](https://gitlab.com/chesedo/caddy-mailout-handler/badges/master/coverage.svg)](https://gitlab.com/chesedo/caddy-mailout-handler/commits/master)

# caddy-mailout-handler
A frontend handler for Caddy's mailout plugin

## Usage
Simply import `Mailout`. It is constructed with two arguments:

- `endpoint` which is the endpoint the plugin is at
- `notifier` which will be called with the result (aka `Message`) of trying to post the data

To send the actual `data` to the endpoint, just call the `send` method on the constructed Mailout.

### `Message`
This object (passed to your `notifier`) has the following structure:
```ts
class Message {
  readonly code: string;
  readonly mes: string;
  hasMailed(): boolean;
}
```

These should be enough to update the UI as needed.

### `data`
It should be an object having at least `email` as key and others will also be handled:
```ts
interface IData {
  email: string;
  name?: string;
  [propName: string]: any;
}
```

## Example
```ts
  import { Mailout, Message } from 'caddy-mailout-handler';

  function notifier(m: Message): void {
    if (m.hasMailed()) {
      alert('Submitted successfully');
    } else {
      alert(`Submition failed: ${m.mes}`);
    }
  }

  const mailout = new Mailout('https://site.com/mailout', notifier);

  // Triggered by some event
  mailout.send({email: 'joe@dow.com', name: 'Joe', subscribe: true});
```
