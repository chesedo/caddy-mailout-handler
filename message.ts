/**
 * Message holds the status of the attempted mailout call
 */
export default class Message {
  constructor(readonly code: number, readonly mes = '') {}
  public hasMailed(): boolean {
    return this.code === 200;
  }
}
